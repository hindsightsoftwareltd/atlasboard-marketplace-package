module.exports = function(config, dependencies, job_callback) {
    var _ = dependencies.underscore;
    var request = dependencies.request;

    var options = {
      url: config.globalAuth.mpac.api + '/vendors/' + config.globalAuth.identity.vendor
    };

    request(options, function(error, response, body) {
      var body = JSON.parse(body);
      var logo = _.where(body.logo.links, { 'rel': 'binary' })[0];
      job_callback(null, { title: config.widgetTitle, imageSrc: logo.href });
    });
};
