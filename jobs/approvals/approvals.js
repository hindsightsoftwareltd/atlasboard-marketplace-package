module.exports = function(config, dependencies, job_callback) {
  var logger = dependencies.logger;
  var request = dependencies.request;

  var options = {
    url: 'https://ecosystem.atlassian.net/rest/api/2/search',
    qs: {
      'jql': 'project=AMKT AND issuetype = Approval AND status not in (closed)',
      'fields': 'status,summary'
    },
    auth: {
      user: config.globalAuth.ean.username,
      pass: config.globalAuth.ean.password,
      sendImmediately: true
    }
  };

  request(options, function(error, response, body) {
    job_callback(null, { title: config.widgetTitle, approvals: JSON.parse(body) });
  });  

}