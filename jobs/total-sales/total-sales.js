function format(date) {
  // yyyy-mm-dd
  return [date.getFullYear(), date.getMonth() + 1, date.getDate() + 1, ].join('-');
}

function now(days) {
  var date = new Date();
  days = days || 0;
  date.setDate(date.getDate() + days);
  return date;
}

module.exports = function(config, dependencies, job_callback) {
    var logger = dependencies.logger;
    var request = dependencies.request;
    var moment = dependencies.moment;

    function dateParam(d) { return moment(d).format('YYYY-MM-DD'); }

    var endDate = moment();
    var startDate = moment(endDate).subtract('weeks', 1);

    var options = {
      url: config.globalAuth.mpac.api + '/vendors/' + config.globalAuth.identity.vendor + '/sales/report/',
      qs: {
        aggregation: config.aggregation,
        'start-date': dateParam(startDate),
        'end-date': dateParam(endDate)
      },
      auth: {
        user: config.globalAuth.mpac.username,
        pass: config.globalAuth.mpac.password,
        sendImmediately: true
      }
    };

    request(options, function(error, response, body) {
      job_callback(null, { title: config.widgetTitle, sales: JSON.parse(body) });
    });
};
