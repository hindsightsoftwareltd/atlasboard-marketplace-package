module.exports = function(config, dependencies, job_callback) {
    var request = dependencies.request;
    var async = dependencies.async;
    var moment = dependencies.moment;

    function dateParam(d) { return moment(d).format('YYYY-MM-DD'); }

    var endDate = moment();
    var startDate = moment(endDate).subtract('weeks', config.weeks);

    function pluginSales(pluginKey, callback) {
      var options = {
        url: config.globalAuth.mpac.api + '/vendors/' + config.globalAuth.identity.vendor + '/sales/report/',
        qs: {
          'add-on': pluginKey,
          'aggregation': config.aggregation,
          'start-date': dateParam(startDate),
          'end-date': dateParam(endDate)
        },
        auth: {
          user: config.globalAuth.mpac.username,
          pass: config.globalAuth.mpac.password,
          sendImmediately: true
        }
      };

      request(options, function(error, response, body) {
        var elements = JSON.parse(body).elements;
        return error ? callback(error) : callback(null, { name: pluginKey, sales: elements });
      });
    }

    async.map(config.globalAuth.identity.pluginKeys, pluginSales, function(err, results) {
      job_callback(null, { title: config.widgetTitle, salesByPlugin: results });
    });
};
