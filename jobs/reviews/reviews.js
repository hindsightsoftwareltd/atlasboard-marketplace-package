module.exports = function(config, dependencies, job_callback) {
    var logger = dependencies.logger;
    var request = dependencies.request;
    var async = dependencies.async;

    // Fetch the latest review for the given plugin key
    function latestReview(pluginKey, callback) {
      var options = {
        url: config.globalAuth.mpac.api + '/plugins/' + pluginKey + '/reviews/',
        qs: { limit: 1 },
        auth: {
          user: config.globalAuth.mpac.username,
          pass: config.globalAuth.mpac.password,
          sendImmediately: true
        }
      };

      request(options, function(error, response, body) {
        var reviews = JSON.parse(body).reviews;
        return error ? callback(error) : callback(null, reviews.length ? reviews[0] : null);
      });
    }

    async.map(config.globalAuth.identity.pluginKeys, latestReview, function(err, results) {
      job_callback(null, { title: config.widgetTitle, reviews: results.slice(0, config.limit) });
    });
};
