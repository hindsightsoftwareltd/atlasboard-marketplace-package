module.exports = function(config, dependencies, job_callback) {
  var logger = dependencies.logger;
  var request = dependencies.request;
  var async = dependencies.async;

  function latestQuestions(pluginKey, callback) {
    var options = {
      url: "https://answers.atlassian.com/api/tags/addon-" + pluginKey + '/tagged/'
    };

    request(options, function(error, response, body) {
      if (response.statusCode === 410) return callback(null, null);
      var questions = JSON.parse(body).tagged_entities;
      return error ? callback(error) : callback(null, questions);
    });
  }

  async.map(config.globalAuth.identity.pluginKeys, latestQuestions, function(err, results) {
    job_callback(null, { title: config.widgetTitle, questions: results });
  });
};
