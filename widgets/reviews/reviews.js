widget = {
    onData: function(el, data) {
        var reviewsTemplate = Mustache.compile($('#reviews-template').html());
        var reviews = data.reviews;

        $('h2', el).text(data.title);

        $('.content', el).append(reviewsTemplate({ reviews: reviews }));

    }
};;
