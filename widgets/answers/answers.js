widget = {
    //runs when we receive data from the job
    onData: function(el, data) {
        var answersTemplate = Mustache.compile($('#answers-template').html());

        var questions = _.chain(data.questions)
            .filter(function(elt){ 
                return !!elt; 
            })
            .flatten(true).sortBy(function(elt) {
                return -moment(elt.last_activity_at).unix();
            })
            .map(function(elt) {
                elt.last_activity_at = moment(elt.last_activity_at).format("MMM Do");
                return elt;
            })
            .value();

        $('h2', el).text(data.title);

        $('.content', el).append(answersTemplate({ questions: questions }));

    }
};;