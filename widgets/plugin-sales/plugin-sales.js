widget = {
    onData: function(el, data) {

        var palette = new Rickshaw.Color.Palette();

        var series = _(data.salesByPlugin).filter(function(ps) { return ps.sales.length > 0; }).map(function(ps) {
            return {
                name: ps.name,
                data: _(ps.sales).map(function(s) {
                    return {
                        x: moment(s.date).unix(),
                        y: s.amount
                    };
                }),
                color: palette.color()
            };
        });

        Rickshaw.Series.zeroFill(series);

        var time = new Rickshaw.Fixtures.Time();

        time.units.push({
            name: 'weekDate',
            seconds: 604800,
            formatter: function(d) {
                return moment(d).format('MMM Do');
            }
        });

        var graph = new Rickshaw.Graph({
            element: $('.graph', el)[0],
            width: 890,
            height: 420,
            renderer: 'bar',
            stroke: true,
            series: series
        });

        var ticksTreatment = 'glow';

        var xAxis = new Rickshaw.Graph.Axis.Time({
            graph: graph,
            ticksTreatment: ticksTreatment,
            timeUnit: time.unit('weekDate')
        });

        var yAxis = new Rickshaw.Graph.Axis.Y( {
            graph: graph,
            orientation: 'right',
            tickFormat: Rickshaw.Fixtures.Number.formatKMBT,
            element: $('.y-axis', el)[0],
        } );

        var legend = new Rickshaw.Graph.Legend({
            element: $('.legend', el)[0],
            graph: graph
        });

        // graph.renderer.unstack = true;
        graph.render();

        $('h2', el).text(data.title);

    }
};