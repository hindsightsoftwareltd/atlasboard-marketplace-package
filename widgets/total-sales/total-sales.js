widget = {
    onData: function(el, data) {

        var lastWeek = data.sales.elements[0].amount;
        var thisWeek = data.sales.elements[1].amount;
        var percent = (thisWeek / lastWeek);
        var delta = (percent < 1 ? -(1.0 - percent) : percent) * 100;

        $('h2', el).text(data.title);
        $('.current', el).text(formatMoney(thisWeek, '$', 0)).toggleClass('negative', delta < 0);
        $('.delta', el).text(formatPercentage(delta, 0)).toggleClass('negative', delta < 0);

    }
};