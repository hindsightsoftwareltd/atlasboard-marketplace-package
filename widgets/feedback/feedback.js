widget = {
    onData: function(el, data) {
        var feedbackTemplate = Mustache.compile($('#feedback-template').html());

        console.log(data.feedback);

        $('.content', el).append(feedbackTemplate({ feedback: data.feedback }));

        $('h2', el).text(data.title);

    }
};